from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    closet_name = models.CharField(max_length=100, unique=True)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hats(models.Model):
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="+",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.name
