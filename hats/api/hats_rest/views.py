from django.http import JsonResponse
from .models import Hats, LocationVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
        "id",
    ]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style",
        "fabric",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {"location": LocationVODetailEncoder()}


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style",
        "fabric",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
           {"hats": hats},
           encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            id = content["location"]
            location = LocationVO.objects.get(id=id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse({"hat": hat},
                            encoder=HatDetailEncoder,
                            )
    elif request.method == "DELETE":
        count,_ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count> 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location Does Not exist"},
                status=400,
        )

        Hats.objects.filter(id=id).update(**content)
        hat = Hats.objects.filter(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
