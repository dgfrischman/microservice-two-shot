# Generated by Django 4.0.3 on 2024-01-31 00:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hats',
            old_name='sytle',
            new_name='style',
        ),
    ]
