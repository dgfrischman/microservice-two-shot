# Wardrobify

Merge around noon and around 530 PM

Team:

* Dylin Frischman - Hats
* Kassandra Vasquez - Shoes

## Design

## Shoes microservice

Create model with the information required that is given on Learn and reviewing wardrobe microservice to review models and views. Creating front end to be organized and creating the Bin VO.

## Hats microservice

Create model based on requirements in rubric, create VO object off existing Location API, create MVP for React, spend additional time if available working on making the front end pretty.
