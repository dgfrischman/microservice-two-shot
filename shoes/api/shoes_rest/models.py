from django.db import models

class BinVO(models.Model):
    import_href = models.CharField(max_length=200)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=40)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name = '+',
        on_delete = models.PROTECT,
    )
    def __str__(self):
        return self.name
