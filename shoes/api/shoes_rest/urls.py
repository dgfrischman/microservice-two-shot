from django.urls import path
from .views import list_shoes, api_show_shoe

urlpatterns = [
    path("shoes/", list_shoes, name="list_shoes"),
    path("shoes/<int:id>/", api_show_shoe, name="api_show_shoe")
]
