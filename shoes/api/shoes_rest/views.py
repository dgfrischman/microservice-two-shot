from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href"
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders = {"bin": BinVODetailEncoder()}


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders = {"bin": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            id = content["bin"]
            bin = BinVO.objects.get(id=id)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({'message': 'Bin does not exist with the id'}, status=400)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoes = Shoe.objects.get(id=id)
        return JsonResponse({"shoes": shoes}, encoder=ShoeDetailEncoder)
    else:
        count,_ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count> 0})
