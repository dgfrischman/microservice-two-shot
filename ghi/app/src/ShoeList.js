import React, { useEffect, useState } from "react";

function ShoeList() {
  const [shoes, setShoes] = useState([]);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8080/api/shoes/");

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (ShoeId) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${ShoeId}`, {
      method: "DELETE",
    });
    if (response.ok) {
      fetchData();
    }
  };

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Picture</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>{shoe.bin.closet_name}</td>
                <td>
                  <img src={shoe.picture_url} alt={shoe.style} />
                </td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => handleDelete(shoe.id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShoeList;
