import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from "react-router-dom"

function UpdateHatForm() {

    const params = useParams();
    const id = params.id
    const navigate = useNavigate()
    const [hat, setHat] = useState('')
    const [style, setStyle] = useState(hat.style);
    const [color, setColor] = useState(hat.color);
    const [fabric, setFabric] = useState(hat.fabric);
    const [picture_url, setPictureUrl] = useState(hat.picture_url);
    const [location, setLocation] = useState(hat.location);
    const [locations, setLocations] = useState([]);



    useEffect(() => {
    const fetchHatData = async () => {
      const response = await fetch(`http://localhost:8090/api/hats/${id}/`);
      try {
      if (response.ok) {
        const data = await response.json();
        setHat(data.hat)
      }
      } catch (e) {
        console.log("Error Fetching Hat Data", e)
      }
    };
        fetchHatData();
    }, [id]);


    useEffect(() => {
      const fetchLocationData = async () => {
        try {
          const response = await fetch('http://localhost:8100/api/locations');
            if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
          }
        } catch (e) {
          console.log('Error fetching locations:', e);
        }
      }

      fetchLocationData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
          fabric,
          style,
          color,
          picture_url,
          location,
        };
        navigate('/hats');


        const hatUrl = `http://localhost:8090/api/hats/${id}/`;
        const fetchConfig = {
          method: 'put',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
          const newHat = await response.json();
          console.log(newHat)
          setFabric('');
          setStyle('');
          setColor('');
          setPictureUrl('');
          setLocation('');
        }
      }

      function handleFabricChange(event) {
        const { value } = event.target;
        setFabric(value);
      }

      function handleStyleChange(event) {
        const { value } = event.target;
        setStyle(value);
      }

      function handleColorChange(event) {
        const { value } = event.target;
        setColor(value);
      }

      function handlePictureUrlChange(event) {
        const { value } = event.target;
        setPictureUrl(value);
      }

      function handleLocationChange(event) {
        const { value } = event.target;
        setLocation(value);
      }


      return (
        <>
        <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Update Hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
            <div className='form-floating mb-3'>
              <input defaultValue={hat.style} onChange={handleStyleChange} placeholder='Style' required type='text' name='style' id='style' className='form-control'/>
              <label htmlFor='style'>Style</label>
            </div>
            <div className="form-floating mb-3">
              <input defaultValue={hat.color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input defaultValue={hat.fabric} onChange={handleFabricChange} placeholder="Fabric" required type="fabric" name="fabric" id="fabric" className="form-control"/>
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input defaultValue={hat.picture_url} onChange={handlePictureUrlChange} placeholder="www.example.com" required type="url" name="picture_url" id="picture_url" className="form-control"/>
              <label htmlFor="picture_url">URL for Picture</label>
            </div>
            <div className='mb-3'>
            <select defaultValue={hat.location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
              <option>Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.closet_name}
                    </option>
                  );
            })};
            </select>
            </div>
            <button className='btn btn-primary'>Update Hat</button>
            </form>
            </div>
        </div>
        </div>
        </>
    );
}
export default UpdateHatForm
