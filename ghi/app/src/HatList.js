import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom"

function HatList() {
    const [hats, setHats] = useState([])

    const fetchData = async () => {
      const response = await fetch('http://localhost:8090/api/hats/');
      try {
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats)
      }} catch(e) {
        console.log("Error fetching hat list")
      }
    };

    useEffect(() => {
      fetchData();
    }, []);


    const handleDelete = async (hatId) => {
      const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
        method: 'DELETE',
      });
      try{
        if (response.ok) {
          fetchData();
      }}catch(e){
        console.log("Error Deleting Item", e)
      }
    }

    return (
        <div>
        <table className='table table-striped'>
          <thead>
            <tr>
              <th>Style</th>
              <th>Color</th>
              <th>Fabric</th>
              <th>Picture</th>
              <th>Location</th>
              <th>Update</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
              return (
                <tr key={hat.id}>
                  <td>{ hat.style }</td>
                  <td>{ hat.color }</td>
                  <td>{ hat.fabric }</td>
                  <td><img src={hat.picture_url} alt={hat.style} style={{ maxWidth: '80px' }}/></td>
                  <td>{ hat.location.closet_name }</td>
                  <td>
                  <Link to={"update/"+hat.id}>
                    <button className="btn btn-warning">Update</button>
                  </Link>
                  </td>
                <td>
                  <button className="btn btn-danger" onClick={() => handleDelete(hat.id)}>Delete</button>
                </td>
              </tr>
            )
          })}
            </tbody>
        </table>
      </div>
    );
}

export default HatList;
