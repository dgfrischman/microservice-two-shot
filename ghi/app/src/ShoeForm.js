import React, { useEffect, useState } from "react";

function ShoeForm() {
  const [manufacturer, setManufacturer] = useState("");
  const [model_name, setModelName] = useState("");
  const [color, setColor] = useState("");
  const [picture_url, setPictureUrl] = useState("");
  const [bin, setBin] = useState("");
  const [bins, setBins] = useState([]);

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };
  const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  };
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };
  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };
  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      manufacturer,
      model_name,
      color,
      picture_url,
      bin,
    };
    console.log(data);

    const ShoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(ShoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      setManufacturer("");
      setModelName("");
      setColor("");
      setPictureUrl("");
      setBin("");
    }
  };
  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <form className="row g-3" onSubmit={handleSubmit}>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="manufacturer">
          Manufacturer
        </label>
        <input
          className="form-control"
          type="text"
          id="manufacturer"
          name="manufacturer"
          placeholder="Manufacturer"
          value={manufacturer}
          onChange={handleManufacturerChange}
        />
      </div>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="model_name">
          Model Name
        </label>
        <input
          className="form-control"
          type="text"
          id="model_name"
          name="model_name"
          placeholder="Model Name"
          value={model_name}
          onChange={handleModelNameChange}
        />
      </div>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="color">
          Color
        </label>
        <input
          className="form-control"
          type="text"
          id="color"
          name="color"
          placeholder="Color"
          value={color}
          onChange={handleColorChange}
        />
      </div>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="picture_url">
          Picture Url
        </label>
        <input
          className="form-control"
          type="text"
          id="picture_url"
          name="picture_url"
          value={picture_url}
          placeholder="Picture URL"
          onChange={handlePictureUrlChange}
        />
      </div>
      <div className="form-group">
        <label className="form-control-label px-3" htmlFor="bin">
          Bin
        </label>
        <select
          value={bin}
          onChange={handleBinChange}
          required
          className="bin form-select"
          id="bin"
        >
          <option value="">Choose a bin</option>
          {bins.map((bin) => (
            <option key={bin.id} value={bin.id}>
              {bin.closet_name}
            </option>
          ))}
        </select>
      </div>
      <div className="form-button mt-3">
        <button type="submit">Submit</button>
      </div>
    </form>
  );
}
export default ShoeForm;
